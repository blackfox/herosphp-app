<?php

// redis session configs

use herosphp\session\RedisSessionHandler;
use Workerman\Protocols\Http\Session\FileSessionHandler;

return [
  'lifetime' => 1800,
  'max_clients' => 2,
  // set Cookie valid, default for current domain
  'domain' => '',
  'secure' => false,
  'http_only' => true,
  // when set it to true, will check the device and ip changes
  'strict_mode' => false,
  // sessionId encrypt private key
  'private_key' => 'R8ZvYP1kIR5X',

  'handler_class' => FileSessionHandler::class,
  'handler_config' => ['save_path' => RUNTIME_PATH . 'session'],
  // 'handler_class' => RedisSessionHandler::class,
  // 'handler_config' => [
  //   'host' => '127.0.0.1', // Required
  //   'port' => 6379,        // Required
  //   'timeout' => 2,           // Optional
  //   'auth' => '',          // Optional
  //   'database' => 1,           // Optional
  //   'prefix' => '_session_'  // Optional
  // ],

];
