<?php
namespace test\service;

use app\service\CalService;
use PHPUnit\Framework\TestCase;

class CalServiceTest extends TestCase
{

    public function testAdd()
    {
        $service = new CalService();
        $this->assertEquals(3,$service->add(1,2),'add errors');
    }
}