<?php

declare(strict_types=1);

namespace app\cmd;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use herosphp\annotation\Action;
use herosphp\annotation\Command;
use herosphp\core\BaseCommand;
use herosphp\core\Input;
use herosphp\utils\Logger;
use herosphp\utils\StringUtil;

#[Command(TestCmd::class)]
class TestCmd extends BaseCommand
{
    #[Action(uri: '/cli/test')]
    public function test(Input $input)
    {
        $name = $input->get('name', 'herosphp');
        Logger::setLevel(Logger::INFO);
        Logger::debug("Task $name start.");

        while ($this->isRunning()) {
            Logger::info("Doing some things here...");
            sleep(1);
        }

        Logger::info("Task down.");
    }

    #[Action(uri: '/cli/jwt')]
    public function jwt()
    {
        $appSecret = StringUtil::genRandomStr(32);
        $appId = "1356999524"; // 企业用户的 AppId
        $payload = [
            'appId' => $appId
        ];
        $jwt = JWT::encode($payload, $appSecret, 'HS256');
        Logger::info("JWT: " . $jwt);
        $decoded = JWT::decode($jwt, new Key($appSecret, 'HS256'));

        Logger::info($decoded);
    }
}
