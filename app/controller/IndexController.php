<?php

declare(strict_types=1);

namespace app\controller;

use app\service\UserService;
use herosphp\annotation\Controller;
use herosphp\annotation\Get;
use herosphp\annotation\Inject;
use herosphp\annotation\Post;
use herosphp\core\BaseController;
use herosphp\core\HttpRequest;
use herosphp\core\HttpResponse;
use herosphp\utils\StringUtil;

#[Controller(IndexController::class)]
class IndexController extends BaseController
{
    #[Inject(name: UserService::class)]
    protected UserService $userSer;

    #[Get(uri: ['/', '/home'])]
    public function index(): HttpResponse
    {
        sleep(30);
        $this->assign('token', StringUtil::genRandomStr(20));
        return $this->json(['uid' => StringUtil::genGlobalUid()]);
    }

    #[Post(uri: '/doPost')]
    public function doPost(): string
    {
        return 'success';
    }

    #[Get(uri: '/user/{userId}')]
    public function user($userId): string
    {
        return $userId;
    }

    #[Get(uri: '/bench')]
    public function bench(HttpRequest $request)
    {
        $session = $request->session();
        $sessionUser = $request->session(1);

        $sessionUser->set('username', 'xiaoming');
        $session->set('name', 'herosphp');

        // clear the session and cookie data
        // $session->clear();
        // $sessionUser->clear();
        return $this->json(['token' => StringUtil::genRandomStr(20), 'name' => $session->get('name'), 'username' => $sessionUser->get('username')]);
    }

    #[Get(uri: '/download')]
    public function downloadFile()
    {
        return $this->download(PUBLIC_PATH . 'test.png', 'test.png');
    }
}
