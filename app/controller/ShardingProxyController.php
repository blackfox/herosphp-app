<?php

declare(strict_types=1);

namespace app\controller;

use herosLdb\Db;
use herosphp\annotation\Controller;
use herosphp\annotation\Get;
use herosphp\core\BaseController;
use herosphp\core\HttpRequest;
use herosphp\utils\StringUtil;

#[Controller(ShardingProxyController::class)]
class ShardingProxyController extends BaseController
{
    #[Get(uri: "/sharding/add")]
    public function add(): string|array
    {
        $user = [
            'id' => StringUtil::genGlobalUid(),
            'username' => "user_" . mt_rand(100, 10000),
            'password' => '12345678',
            'created_at' => date('Y-m-d H:i:s')
        ];
        if (Db::table("t_user")->insert($user)) {
            return $user;
        } else {
            return "插入失败";
        }
    }

    #[Get(uri: "/sharding/get")]
    public function get(HttpRequest $request): \Illuminate\Support\Collection
    {
        $id = $request->get('id');
        if (!$id) {
            $id = 167703693701476181;
        }
        return Db::table('t_user')->where('id', '=', $id)->get();
    }

    #[Get(uri: '/sharding/list')]
    public function list(HttpRequest $request): \Illuminate\Support\Collection
    {
        $page = $request->get('page', 1);
        return Db::table('t_user')->where('id', '>', 100)->forPage($page, 20)->get();
    }
}
