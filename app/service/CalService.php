<?php

namespace app\service;

use herosphp\annotation\Service;

#[Service(name: CalService::class)]
class CalService
{
    public function add(int $a, int $b):int
    {
        return $a + $b;
    }
}
