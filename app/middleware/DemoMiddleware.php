<?php

declare(strict_types=1);

namespace app\middleware;

use herosphp\core\HttpRequest;
use herosphp\core\HttpResponse;
use herosphp\core\MiddlewareInterface;
use herosphp\utils\Logger;

class DemoMiddleware implements MiddlewareInterface
{
    public function process(HttpRequest $request, callable $handler): HttpResponse
    {
        Logger::info("Enter Demo Middleware, uri: " . $request->uri());
        return $handler($request);
    }
}
